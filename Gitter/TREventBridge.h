//
//  TREventBridge.h
//  Troupe
//
//  Created by Andrew Newdigate on 06/02/2013.
//  Copyright (c) 2013 Troupe Technology Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TREventController.h"

@interface TREventBridge : NSObject

@property (readonly, nonatomic) BOOL subscriptionIsReady;

+ (TREventBridge *) sharedInstance;

- (void) applicationDidEnterBackground;

- (void) applicationWillTerminate;

- (void) applicationWillEnterForeground;


@end
