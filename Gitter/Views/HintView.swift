import Foundation

class HintView: UIView {

    var delegate: HintViewDelegate?

    @IBOutlet var button: UIButton!

    @IBAction func didTapButton(_ sender: UIButton) {
        delegate?.didTapButton(self, button: sender)
    }
}

protocol HintViewDelegate {
    func didTapButton(_ hintView: HintView, button: UIButton)
}
