import Foundation
import WebKit
import CoreData

class TopicsViewController: UIViewController {

    var groupId: String?

    private var webView: WKWebView?
    private var group: Group? {
        didSet {
            if let uri = group?.uri {

                let url = URL(string: "https://gitter.im/\(uri)/topics")!
                let request = NSMutableURLRequest(url: url)
                request.addValue("Bearer \(LoginData().getAccessToken()!)", forHTTPHeaderField: "Authorization")
                if (SecretMenuData().useStagingApi) {
                    request.addValue("gitter_staging=staged;", forHTTPHeaderField: "Cookie")
                }

                let _ = webView?.load(request as URLRequest)
            }
        }
    }

    override func loadView() {
        super.loadView()

        webView = WKWebView()
        webView!.scrollView.bounces = false
        webView!.translatesAutoresizingMaskIntoConstraints = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(webView!)
        let viewDict: [String : AnyObject] = [ "webView": webView! ]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webView]|", options: [], metrics: nil, views: viewDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[webView]|", options: [], metrics: nil, views: viewDict))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        group = getGroupModel(groupId!)

        RestService().getGroup(byId: groupId!, completionHandler: { (error, didSaveNewData) in
            guard error == nil else {
                LogError("failed to get group from network", error: error!)
                return
            }
            if self.group == nil {
                self.group = self.getGroupModel(self.groupId!)
            }
        })
    }

    private func getGroupModel(_ id: String) -> Group? {
        let uiMoc = CoreDataSingleton.sharedInstance.uiManagedObjectContext
        let fetchRequest = NSFetchRequest<Group>()
        let entity = NSEntityDescription.entity(forEntityName: "Group", in: uiMoc)
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        var group: Group?
        do {
            let result = try uiMoc.fetch(fetchRequest)
            group = result.last
        } catch {
            LogError("Failed to get group from core data", error: error)
        }
        return group
    }
}
