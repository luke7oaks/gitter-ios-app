import Foundation
import CoreData

class PickCommunityViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var delegate: PickCommunityViewControllerDelegate?
    var community: Group? {
        didSet {
            delegate?.didChangeCommunity(self, community: community)
        }
    }
    private let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var fetchedResultsController: NSFetchedResultsController<Group>?

    override func loadView() {
        super.loadView()

        let adminGroupRequest = NSFetchRequest<Group>(entityName: "Group")
        adminGroupRequest.predicate = NSPredicate(format: "adminGroupCollection != NULL")
        adminGroupRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: adminGroupRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController!.delegate = self

        RestService().getAdminGroups { (error, didSaveNewData) in
            LogIfError("failed to get admin groups", error: error)
        }
        try! fetchedResultsController?.performFetch()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AvatarCell", bundle: nil), forCellReuseIdentifier: "AvatarCell")
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        TableViewChangeHandler.applyRowChange(to: tableView, at: indexPath, for: type, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    @IBAction func didTapCreateNewCommunityButton(_ sender: UIButton) {
        delegate?.didIntendToCreateNewCommunity(self)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? fetchedResultsController!.sections![section].numberOfObjects : 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarCell", for: indexPath) as! AvatarCell
            configureCell(cell, indexPath: indexPath)
            return cell
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "CreateButtonCell", for: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        community = fetchedResultsController!.object(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadData()
    }

    private func configureCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == 0 {
            let cell = cell as! AvatarCell
            let group = fetchedResultsController!.object(at: indexPath)

            AvatarUtils.sharedInstance.configure(cell.avatar, group: group, size: 39)

            cell.title.text = group.name
            cell.accessoryType = group.id == self.community?.id ? .checkmark : .none
        }
    }
}

protocol PickCommunityViewControllerDelegate {
    func didChangeCommunity(_ pickCommunityViewController: PickCommunityViewController, community: Group?)
    func didIntendToCreateNewCommunity(_ pickCommunityViewController: PickCommunityViewController)
}
