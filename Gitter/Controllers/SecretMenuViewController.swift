import Foundation

class SecretMenuViewController: UITableViewController {

    @IBOutlet var prodCell: UITableViewCell!
    @IBOutlet var prodStagingCell: UITableViewCell!
    @IBOutlet var topicsSwitch: UISwitch!

    private let settings = SecretMenuData()

    private var betaStagingBranch: String?

    @IBAction func topicsSwitchDidToggle(_ sender: UISwitch) {
        settings.showTopics = topicsSwitch.isOn
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)

        switch cell {
        case prodCell:
            cell.accessoryType = !settings.useStagingApi ? .checkmark : .none
        case prodStagingCell:
             cell.accessoryType = settings.useStagingApi ? .checkmark : .none
        default:
            topicsSwitch.isOn = settings.showTopics
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        switch cell {
        case prodCell:
             settings.useStagingApi = false
        case prodStagingCell:
            settings.useStagingApi = true
        default:
            return
        }

        tableView.reloadData()
    }
}
