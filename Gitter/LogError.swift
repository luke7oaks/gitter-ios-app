import Foundation

func LogIfError(_ message: String, error: Error?) {
    if let error = error {
        LogError(message, error: error)
    }
}

func LogIfError(_ message: String, error: NSError?) {
    if let error = error {
        LogError(message, error: error)
    }
}

func LogError(_ message: String, error: Error) {
    LogError(message, error: error as NSError)
}

func LogError(_ message: String, error: NSError) {
    NSLog("\(message); \(error), \(error.userInfo)")
}
